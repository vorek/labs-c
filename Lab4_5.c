#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define maxString 10
FILE *fp;
void chomp (char *str)
{
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=0;
}
int cmp (const void *a, const void *b)
{
	return (strlen(*(char **)a)-strlen(*(char **)b));
}
int main(int argc, char **argv)
{
	int i,count=0;
	char buf[maxString][256];
	char *c[maxString];
	if (argc < 2)
	{
		puts("Usage: warehouse.exe filename_input.txt filename_output.txt");
		return 0;
	}
	
	fp = fopen(argv[1], "rt");
	if (fp == 0)
		return 0;
	while ((!feof(fp))&&(count<maxString)) // ���� ���� �� ����������
	{
		fgets(buf[count],256,fp);
		chomp(buf[count]);
		c[count]=buf[count];
		count++;
	}
	fclose(fp);
		
	qsort(c,count,sizeof(char*),cmp);
	fp=fopen(argv[2], "wt");
	if (fp == 0)
		return 0;
	
	for (i=0;i<count;i++)
		fprintf (fp,"%s\n",c[i]);
	fclose(fp);
	return 0;
}