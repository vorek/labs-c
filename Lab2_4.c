#include <stdio.h>
#include <time.h>
#include <stdlib.h>
#include <string.h>

void swap(char *begin, char *end)
{
	char temp=*begin;
	*begin=*end;
	*end=temp;
}
void chomp(char *str)
{
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=0;
}

char str[50];
int i,flag;

main()
{
	char *begin,*end;
	srand((unsigned int)time(0));
	for (i=0;i<50;i++)
	{
		flag=1;
		do
		{
			str[i]=rand()%100+48;
    		if ((str[i]>='1')&&(str[i]<='9')||((str[i]>='A')&&(str[i]<='Z'))||((str[i]>='a')&&(str[i]<='z')))
				flag=0;
			
		}
		while (flag);
		
	}
	chomp(str);
    printf("%s\n",str);
	begin=str;
	end=&str[strlen(str)-1];
	
	while (begin!=end)
	{
		if ((*begin>='0')&&(*begin<='9'))
		{
				swap(begin,end);
				end--;
		}
		else begin++;
	}
	printf("%s\n",str);
	return 0;
}