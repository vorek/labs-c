#include <stdio.h>
#include <string.h>
#include <stdlib.h>

void PrintWord(char *word) //Печать последовательности
{
    while (*word == *(word+1))
        putchar(*word++);
    putchar(*word);
}
int WordLen(char *word) //Вычисление длины последовательности
{
    int len=0;
    while (*word == *(word+1))
    {    
        len++;
        word++;
    }
    return len+1;
}
void chomp(char *buf)
{
    if (buf[strlen(buf)-1]=='\n')
        buf[strlen(buf)-1]=0;
}


main()
{
    char buf[80],*beg,*end,*bw;
    char *arr[80];
    int count=0,i=0,flag=0;
    printf("Enter string:\n");
    fgets(buf,80,stdin);
    chomp(buf);

    while (buf[i])
    {
        if ((buf[i]==buf[i+1])&&(flag==0))
        {
            flag=1;
            arr[count++]=buf+i;
        }
        else if (buf[i]!=buf[i+1] && flag==1)
        {
            flag=0;
        }
        i++;
    }
    bw=arr[0];
    for (i=1;i<count;i++)
        if (WordLen(arr[i])>WordLen(bw))
            bw=arr[i];
    if (count==0)
    {
        printf("The biggest sequence: %c, 1 letter\n",buf[0]);
        return 0;
    }
    printf("The biggest sequence: ");
    PrintWord(bw);
    printf (", %d letters\n",WordLen(bw));
    return 0;
}