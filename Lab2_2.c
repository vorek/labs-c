#include <stdio.h>
#include <stdlib.h>
#include <time.h>

int num, p;
int flag=0,count=1,test;

int shot()
{
	int EnterShot=0;
	do
	{
		fflush (stdin);
		printf("Enter a shot from 1 till 100: ");
		test=scanf("%d",&EnterShot);
		if (!test)
			printf("Wrong number, try again. ");
	}
	while (!test);

	return EnterShot;
}


main()
{
	srand((unsigned int)time(0));
	num=rand()%100;
	do
	{
		p=shot();
		if (p==num)
			{
				printf("BINGO! You guess in %d attempts\n",count);
				flag=1;
		}
		else 
			printf("%s",p>num?"Smaller\n":"Bigger\n");
		count++;
	}
	while (!flag);

	return 0;
}