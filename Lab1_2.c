#include <stdio.h>

int main()
{
	int hh,mm,ss,result=0;
	printf ("Enter date HH:MM:SS ");
	//scanf("%d:%d:%d",&hh,&mm,&ss);
	result=scanf("%d:%d:%d",&hh,&mm,&ss);
	if ((result<3)||(hh>24)||(mm>59)||(ss>59))
	{
		printf("Wrong date\n");
		return 1;
	}
	if (hh<8)
		printf("Good night!\n");
	if ((hh>=8)&&(hh<12))
		printf("Good morning!\n");
	if ((hh>=12)&&(hh<16))
		printf("Good day!/n");
	if (hh>=16)
		printf("Good evening!\n");

	return 0;
}

