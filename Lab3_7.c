#include <stdio.h>
#include <stdlib.h>

struct SYMBOLS
{
    char symb; //Символ
    int count; //Количество
};

void chomp(char *buf)
{
    if (buf[strlen(buf)-1]=='\n')
        buf[strlen(buf)-1]=0;
}

int main()
{
    struct SYMBOLS mass[80];
    char buf[80];
    int i,j,N=1,flag,max,pos;
    printf("Enter string:\n");
    fgets(buf,80,stdin);
    chomp(buf);
        
    mass[0].symb=buf[0];
    mass[0].count=1;

    for (i=1;i<strlen(buf);i++)
    {
        flag=0;
        for (j=0;j<N;j++)
            if (mass[j].symb==buf[i])
            {
                mass[j].count++;
                break;
            }
            else if (j==N-1)
            {
                mass[N].symb=buf[i];
                mass[N].count=1;
                flag=1;
            }
        if (flag)
            N++;
    }

    for (i=0;i<N;i++)
    {
        max=0;
        for (j=0;j<=N;j++)
        {
            if (mass[j].count>max)
            {
                max=mass[j].count;
                pos=j;
            }
        }
        printf("%c  - %d\n",mass[pos].symb,mass[pos].count);
        mass[pos].count=0;
    }

    return 0;
}