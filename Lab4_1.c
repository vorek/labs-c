#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define maxString 5

void chomp (char *str)
{
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=0;
}
int cmp (const void *a, const void *b)
{
	return (strlen(*(char **)a)-strlen(*(char **)b));
}
int main()
{
	int i,count=0;
	char buf[maxString][80];
	char *c[maxString];
	printf ("Enter strings (max %d):\n",maxString);
	do
	{
		fgets(buf[count],80,stdin);
		chomp(buf[count]);
		c[count]=buf[count];
	
	}
	while (strlen(buf[count++]));
	qsort(c,count-1,sizeof(char*),cmp);
	for (i=0;i<count-1;i++)
		printf ("%s\n",c[i]);
	return 0;
}