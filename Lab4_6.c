#include <stdio.h>
#include <string.h>
#include <stdlib.h>

int N,i,buf,min=0,max=0;
char *young,*old;
	char arr[40][40];

int EnterAndTestN ()
{
    int test=0;
    do
    {
        fflush(stdin);
        printf("Enter number of relatives: ");
        test=scanf("%d",&N);
        if ((!test)||(N<=0))
            printf("Wrong number. Try again. ");
    }
    while((!test)||(N<=0));

    return N;
}
int main()
{
	N=EnterAndTestN();
	young=old=NULL;
	for (i=0;i<N;i++)
	{
		printf("Enter %d relative's name: ",i+1);
		scanf("%s",arr[i]);
		printf("Enter %s's age: ",arr[i]);
		scanf("%d",&buf);
		if ((buf<min)||(i==0))
		{
			min=buf;
			young=arr[i];
		}
		else if (buf>max)
		{
			max=buf;
			old=arr[i];
		}
	}
	printf ("Oldest: %s, his age is %d;\nyoungest: %s, his age is %d\n",old,max,young,min);
	
	return 0;
}