#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <time.h>

char *arr[80],buf[80];
int i=0, N=0, temp;
void PrintWord(char *word)
{
    while (*word && *word!=' ')
        putchar(*word++);
}
void chomp(char *buf)
{
    if (buf[strlen(buf)-1]=='\n')
        buf[strlen(buf)-1]=0;
}
int GetWords(char *buf, char *arr[80])
{
	int flag=0,count=0;
	while (*buf)
    {
        if ((*buf!=' ')&&(flag==0))
        {
            flag=1;
            arr[count++]=buf;
        }
        else if (*buf==' ' && flag==1)
        {
            flag=0;
        }
        buf++;
    }
	return count;
}

int main()
{
	
	int i;
	srand((unsigned int)time(0));
	printf("Enter string:\n");
    fgets(buf,80,stdin);
    chomp(buf);
	temp=N=GetWords((char*)buf,arr);
	while(temp)
	{
		int t;
		t=(rand()%N);
		if (arr[t]!=NULL)
		{
			PrintWord(arr[t]);
			putchar(' ');
			arr[t]=NULL;
			temp--;
		}
	}
	putchar ('\n');
	return 0;
}