#include <stdio.h>

int main()
{
	char angle=0;
	double g=0;
	double result=0;
	
	printf("Enter angle 00.00D or 00.00R, where D - Decimal, R - Radian: ");
	scanf("%lf%c",&g,&angle);
		
	if ((angle=='d')||(angle=='D'))
	{
		result=g*3.1415926535898/180;
		printf("Your angle=%.2f radian\n",result);
		return 0;
	}
	if ((angle=='r')||(angle=='R'))
	{
		result=g*180/3.1415926535898;
		printf("Your angle=%.2f decimal\n",result);
		return 0;
	}

	printf("Wrong data\n");
	return 0;
}