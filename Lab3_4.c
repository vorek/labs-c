#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#define N 5

main()
{
    char buf[80],tempword[80];
    int i=0,j,flag=0,sum=0;
    printf("Enter string:\n");
    fgets(buf,80,stdin);

    while (buf[i])
    {
        if ((buf[i]>='0')&&(buf[i]<='9')&&(flag<N))
            tempword[flag++]=buf[i];
        else if ((buf[i]<'0' || buf[i]>'9') || flag>=N)
        {
            sum+=atoi(tempword);
            for (j=0;j<flag;j++)
                tempword[j]=' ';
            if (flag==N)
                i--;
            flag=0;
        }
        i++;

    }
    if (sum==0)
        printf ("There are no numbers in the string\n");
    else 
        printf ("Summa=%d\n", sum);
    return 0;
}