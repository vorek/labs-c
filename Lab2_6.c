#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char str[80];
char *beg, *end, *i;

void chomp (char *str)
{
    if (str[strlen(str)-1]=='\n')
        str[strlen(str)-1]=0;
}
void MoveSpaces (char *beg, char *end)
{
    i=beg;
    while (i<end)
    {
        *i=*(i+1);
        i++;
    }
    *end=0;
    end--;
}
void DelFirstSpace (char *beg, char *end)
{
    int flag=0;
    do
    {
        if (*beg==' ')
            MoveSpaces(beg,end);
        else
            flag=1;
    }
    while (!flag);
}

int main()
{
    printf ("Enter string:\n");
    fgets(str,80,stdin);
    chomp(str);
    end=&str[strlen(str)-1];
    beg=str;
    DelFirstSpace(beg,end);
    while (beg!=end)
        if ((*beg==' ')&&(*(beg+1)==' '))
            MoveSpaces(beg,end);
        else 
            beg++;
    printf("New string: \n%s\n",str);
    return 0;
}