#include <stdio.h>
#include <time.h>
#include <stdlib.h>

int N,i,sum=0;
int beg,end,max,min,*arr=NULL;

int EnterAndTestN ()
{
    int test=0;
    do
    {
        fflush(stdin);
        printf("Enter number of elements: ");
        test=scanf("%d",&N);
        if ((!test)||(N<=0))
            printf("Wrong number. Try again. ");
    }
    while((!test)||(N<=0));

    return N;
}

int FindMax(int *arr,int N) //����� ������� ������������� ����� � �������
{
    end=0;
    for (i=1;i<N;i++)
        if (arr[i]>arr[end])
            end=i;
    return end;
}

int FindMin(int *arr,int N) //����� ������� ������������ ����� � �������
{
    beg=0;
    for (i=1;i<N;i++)
        if (arr[i]<arr[beg])
            beg=i;
    return beg;
}

int Summa(int *arr, int beg, int end)
{
    for (i=beg+1;i<end;i++)
        sum=sum+arr[i];
    return sum;
}

int main()
{
    
    srand((unsigned int)time(0));
    N=EnterAndTestN();
    arr=(int*)malloc(N * sizeof(*arr));
    for (i=0;i<N;i++)
    {
        arr[i]=rand()%100-50;
        printf("%d\n",arr[i]);
    }
    
    beg=FindMin(arr,N);
    end=FindMax(arr,N);
    if (beg==end)
    {
        printf("Error, numbers not found\n");
        return 0;
    }
    if (beg<end)
        sum=Summa(arr,beg,end);
    else 
        sum=Summa(arr,end,beg);
    printf ("Max number: %d, Min number: %d. Summa between max and min=%d\n",arr[end],arr[beg],sum);

    free(arr);
    return 0;
}