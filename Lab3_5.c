#include <stdio.h>
#include <time.h>
#include <stdlib.h>


int N,i,sum=0;
int beg,end,*arr=NULL;

int EnterAndTestN ()
{
    int test=0;
    do
    {
        fflush(stdin);
        printf("Enter number of elements: ");
        test=scanf("%d",&N);
        if ((!test)||(N<=0))
            printf("Wrong number. Try again. ");
    }
    while((!test)||(N<=0));

    return N;
}

int FindFirst(int *arr,int N) //����� ������� �������������� ����� � �������
{
    for (i=0;i<N;i++)
        if (arr[i]<0)
            return i;
    return N;
}

int FindLast(int *arr,int N) //����� ���������� �������������� ����� � �������
{
    for (i=N;i>=0;i--)
        if (arr[i]>0)
            return i;
    return 0;
}

int main()
{
    
    srand((unsigned int)time(0));
    N=EnterAndTestN();
    arr=(int*)malloc(N * sizeof(*arr));
    for (i=0;i<N;i++)
    {
        arr[i]=rand()%100-50;
        printf("%d\n",arr[i]);
    }
    
    beg=FindFirst(arr,N);
    end=FindLast(arr,N);
    if (beg>=end)
    {
        printf("Error, numbers not found\n");
        return 0;
    }
    for (i=beg+1;i<end;i++)
        sum+=arr[i];
    printf ("First negative: %d, Last pozitive: %d. Summa between first negative and last pozitive=%d\n",arr[beg],arr[end],sum);

    free(arr);
    return 0;
}