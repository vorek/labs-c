#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char buf[80];
int i,count=0;

void chomp(char *buf)
{
    if (buf[strlen(buf)-1]=='\n')
        buf[strlen(buf)-1]=0;
}

int main()
{
    printf("Enter string:\n");
    fgets(buf,80,stdin);
    chomp(buf);
    if (buf[0]!=' ')
        count=1;
    for (i=1;i<strlen(buf)-1;i++)
        if (buf[i]==' ' && buf[i+1]!=' ')
            count++;
    printf("There are %d words\n",count);
    return 0;
}